package com.cs211d.workwithsd;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.Scanner;


public class SdCard extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sd_card);

        try
        {
            String str = "to was cold and windy";
            File sdCard = Environment.getExternalStorageDirectory();
            // you can call any name for your folder
            // absolute: from the root to the end
            //create own folder
            File dir = new File(sdCard.getAbsolutePath()+"/MyFiles");

            // if it does not exist
            //make sure to create it
            if(!dir.exists())
            {
                dir.mkdirs();
            }
            // create the file
            File f = new File(dir,"textFile.txt");

            // write into it
            FileOutputStream fos = new FileOutputStream(f);

            // opent the file
            // only my application can get into the file:
            // be care ful this will create the same file if called multiple
            FileOutputStream fos1 = openFileOutput("myfile.txt", Context.MODE_PRIVATE);
            // to abppend
            FileOutputStream fos2 = openFileOutput("myfile.txt", Context.MODE_APPEND);
            // everyone can read
            FileOutputStream fos3 = openFileOutput("myfile.txt", Context.MODE_WORLD_READABLE);
            // everyone can write
            FileOutputStream fos4 = openFileOutput("myfile.txt", Context.MODE_WORLD_WRITEABLE);

            // convert the string to bytes
            fos2.write(str.getBytes());
            fos2.flush();
            fos2.close();

            // read from the file
            FileInputStream fis = openFileInput("myfile");
            // we can also use a scanner
            Scanner sc = new Scanner(fis);
            while(sc.hasNext())
            {
                //read from it
                Log.i("from the file",sc.next());
            }

            fis.close();
            sc.close();

            if (externs)
            {

            }

            OutputStreamWriter osw = new OutputStreamWriter(fos);
            osw.write(str);
            osw.flush();
            osw.close();

        }catch (IOException e) {
            e.printStackTrace();
        }

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_sd_card, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
