package com.cs211d.testapplication4D;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

public class MainActivity4 extends Activity
{
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState)
    {	
        super.onCreate(savedInstanceState);
        
        //this line needs to be here to remove title from app
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        
        setContentView(R.layout.main);

//		Access array
		Resources r = getResources();
		String str[] = r.getStringArray(R.array.buf);
		
		for (String string : str) {
			Log.d("array", string+"");
		}
    	TextView tv = (TextView) findViewById(R.id.line);
//    	TextView tv2 = (TextView) findViewById(R.id.line);
//    	set the textview object text
		tv.setText("This is line1");
		tv.setTextSize(getResources().getDimension(R.dimen.reg));
//		tv2.setText("This is line2");
		
		
    }
}
