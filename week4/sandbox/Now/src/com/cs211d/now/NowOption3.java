package com.cs211d.now;

import java.util.Date;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;

public class NowOption3 extends Activity 
{
	//search for button in xml file
	//find xml file with the one with t butotn tag
	//in main.xml
	 Button btn = (Button)findViewById(R.id.mybtn);
	
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

    	
        requestWindowFeature(Window.FEATURE_NO_TITLE);
   
        
        setContentView(R.layout.main);
        
    }
    
    public void doit(View v)
    {
    	switch(v.getId())
    	{
    		case R.id.mybtn:
    			update();
    		break;
    		case R.id.mybtn2:
    			//copyfile()
    		break;
    	}
    }
    /**
     * update the time of the button
     */
    private void update()
    {
    	/**
    	 * the button class is sub classing 
    	 * text view, what ever textview 
    	 * has, button has
    	 * 
    	 * set textview puts label to the button
    	 */
    	btn.setText(new Date().toString());
    }

	
}
