package com.cs211d.now;

import java.util.Date;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;

public class NowOption1 extends Activity implements OnClickListener, android.view.View.OnClickListener
{
	Button btn;
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        
        //object of the class that implements
        //onclicklistener
        btn = new Button(this);
        //this object of the class that implements 
        //onclick listener will transfer to onclick
        // catch the event
        btn.setOnClickListener(this);
        
        //my entire screen is my button
        setContentView(btn);
        
//        setContentView(R.layout.main);
    }
    public void onClick(View v)
    {
    	update();
    }
    
    /**
     * update the time of the button
     */
    private void update()
    {
    	/**
    	 * the button class is sub classing 
    	 * text view, what ever textview 
    	 * has, button has
    	 * 
    	 * set textview puts label to the button
    	 */
    	btn.setText(new Date().toString());
    }
    
	@Override
	/**
	 * The on click method
	 * @param dialog - 
	 * @param which - 
	 */
	public void onClick(DialogInterface dialog, int which) {
		// TODO Auto-generated method stub
		
	}
}
