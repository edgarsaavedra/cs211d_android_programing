package com.cs211d.now;

import java.util.Date;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;

public class NowOption2 extends Activity 
{
	//search for button in xml file
	//find xml file with the one with t butotn tag
	//in main.xml
	 Button btn = (Button)findViewById(R.id.mybtn);
	
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

    	
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        
        //handle the event, we need to plass object
        //of the class that implements onclicklistener
        //creating anonymous inner class to handle event 
        // in android without having to name the object
        
        
        btn.setOnClickListener(new View.OnClickListener() {
			
			public void onClick(View v) {
				// TODO Auto-generated method stub
				update();
			}

			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				
			}
		    /**
		     * update the time of the button
		     */
		    private void update()
		    {
		    	/**
		    	 * the button class is sub classing 
		    	 * text view, what ever textview 
		    	 * has, button has
		    	 * 
		    	 * set textview puts label to the button
		    	 */
		    	btn.setText(new Date().toString());
		    }
		});
        
        setContentView(R.layout.main);
        
    }

	
}
