package cs211d.com.chpater6;

import android.app.Activity;
import android.os.Bundle;
import android.text.util.Linkify;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AutoCompleteTextView;
import android.widget.TextView;

import org.w3c.dom.Text;


public class Chapter6 extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

//        setContentView(R.layout.activity_chapter6);
        setContentView(R.layout.controls);

//        get text view
//        TextView nameValue = (TextView)findViewById(R.id.nameValue);
//        nameValue.setText("edgarrrrr!!!");
//        TextView addrValeu = (TextView)findViewById(R.id.addrValue);
//        addrValeu.setText("theaddress 99999000");

//        controls
//        auto link
        TextView tv = (TextView)findViewById(R.id.tv);
//        tv.setAutoLinkMask(Linkify.ALL);
        tv.setText("Please visit my website, http://www.androidbook.com or email me at edgar@edgarsaavedra.com");

        AutoCompleteTextView actv = (AutoCompleteTextView) findViewById(R.id.actv);


    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_chapter6, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
