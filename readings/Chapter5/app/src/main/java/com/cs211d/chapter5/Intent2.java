package com.cs211d.chapter5;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

/**
 * Created by edgar on 3/11/15.
 */
public class Intent2 extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.intent2);

        Bundle extraBundle = getIntent().getExtras();
        TextView tv = (TextView)findViewById(R.id.text2);
        tv.setText(extraBundle.get("extra1")+"");

//        Intent i = new Intent();
//        i.putExtra("name","ED!!GAR!!!");
//        setResult(RESULT_OK,i);
//        finish();
    }    @Override
         public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {

//            Bundle extraBundle = getIntent().getExtras();
//            TextView tv = (TextView)findViewById(R.id.text1);
//            tv.setText(extraBundle.get("extra1")+"");

            Intent i = new Intent();
            i.putExtra("name","thename!");
            setResult(RESULT_OK,i);
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
