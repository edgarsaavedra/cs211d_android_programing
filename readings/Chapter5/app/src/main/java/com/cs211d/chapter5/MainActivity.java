package com.cs211d.chapter5;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;


public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {

//            Toast t = new Toast(getApplicationContext()).makeText(getApplicationContext(),item.getItemId()+"--",Toast.LENGTH_SHORT);
//            t.show();

//            implicit intent
//            IntentUtils.tryOneOfThese(this);

//            explicit intent
            Intent intent = new Intent(getApplicationContext(),Intent2.class);

//            using bundles
            Bundle sendToIntent2 = new Bundle();
            sendToIntent2.putString("extra1","thisdatafrom---thefirstextra");
            intent.putExtras(sendToIntent2);
//            startActivity(intent);

            startActivityForResult(intent,1778788787);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent outputIntent)
    {
//        this is to inform that the the called activity has finised
//        super.onActivityResult(requestCode,resultCode,outputIntent);



        Bundle s = outputIntent.getExtras();
        if (resultCode == RESULT_OK && requestCode == 1778788787)
        {
            TextView tv = (TextView) findViewById(R.id.text1);
            String t2 = s.getString("name");
            Toast t = new Toast(getApplicationContext()).makeText(getApplicationContext(),t2+" is the result code",Toast.LENGTH_SHORT);
            t.show();
//            tv.setText(bun.getString("name"));

        }

    }
}
