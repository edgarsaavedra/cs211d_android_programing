We were talking about sql lite.

1) create a database
2) Make sure to create statements to create a
database.
3) once database is created you need to create a table
 -- tables are nothing but rows and columns

================================= in oncreate =================================

SQLiteDatabase db;


        SQLiteDatabase db;
        //create the database
        // there are other flags:
//        SQLiteDatabase.OPEN_READONLY;
//        SQLiteDatabase.OPEN_READWRITE
        // this will create the database in: /data/data/com.cs211d/databases/mydb.db
        db = openOrCreateDatabase("mydb.db",SQLiteDatabase.CREATE_IF_NECESSARY,null);
        //set up locking and version number
        // prevents database corruption
        db.setLockingEnabled(true);
        //start from 1 and increment by 1
        db.setVersion(1);

        // the sql command to execute
        // execsql is a sql command that does
        //not return anything

        //drop a table
//        String sqlcmd = "drop table in foo;";

        //sql command to add a table
//        String sqlcmd = "create table infotable (_id integer"+"primary";

//        String sqlcmd = "CREATE TABLE infotable"+
//                " ( id INTEGER PRIMARY KEY,"+
//                "name TEXT,"+
//                "age INTEGER,"+
//                "dep TEXT);";

        // create the table and execute our command
//        db.execSQL(sqlcmd);

        // ========= normally in a loop

            //after create a table we use the insert command to
            //populate the table, we need to  create an object
            ContentValues cv = new ContentValues();
            cv.put("name","Edgar");
            cv.put("age",25);
            cv.put("dep","dev");

            // we insert the data this tells you
            // how many records are processed
            // this should be in a throw
            long recNum = db.insert("infotable",null,cv);

        // ========= normally in a loop

        /*
        * =======================================
        * **************************************
        * UPDATE WITH  WHERE CLAUSE
        * **************************************
        * =======================================
        */

        // update a table
        /* db.update --
        *  db.update has 4 parameters
        *  update(String tableName, ContentValue object, [optional where clase, could be null], [Array arrayOfWhereClause or null]);
        *
        *  where clause is the limit to certain key or group
        *  if argument #3 & 4 are null it will mean all records are affected
        *  exmaple:
        * */

        //1 create content value

        ContentValues cv2 = new ContentValues();
        cv2.put("age",30);

        // for who?, for john


        // name=? is equivilant is a where clause,
        // make sure there is a question mark,
        // ? is a placeholder for data
        // normally in sql where name = 'John'


//        db.update("infotable", cv, "name=?", new String[]{"John"});

        /*
        *
        * I Sql we are using this to update
        * update info age = 30 where name = 'John'
        *
        * */

        /*
        * =======================================
        * **************************************
        * UPDATE WITH MULTIPLE WHERE CLAUSE
        * **************************************
        * =======================================
        *
        * We want to be more specific and require another where
        * clause
        *
        * db.update("infotable", cv, "name=? and dep=?", new String[]{"John","HR"});
        * */
        db.update("infotable", cv, "name=? and dep=?", new String[]{"Edgar","dev"});

        /*
        *
        * =======================================
        * **************************************
        * DELETING RECORDS
        * **************************************
        * =======================================
        *
        * To delete the entire table in database
        * ===========================================
        * delete info;
        *
        * If we want to delete in SQL we run this query
        * ============================================
        *
        * delete info where dept="ENG";
        *
        * THis is how we delete record in android
        * delete method:
        * delet(String tablename, String whereClause, Array whereArgument)
        * exmple:
        * */
        //delete record with david as a name in it


//         db.delete("infotable","name=?",new String[]{"David"});


        /*
        * =======================================
        * **************************************
        * RUNNIG QUERY
        * **************************************
        * =======================================
        */

        /*
           all of the data that is part of a query
           will be collected in memory and will be
           saved in memory. A query returns a value

           there is a pointer that points to the line
           above the very first record. This pointer is
           called a cursor. Cursor is a pointer that highlights
           where the data is residing in memory. You will
           be moving the pointer to the next record

           We will retrieve the first record, after retrieving
           we will advance to the next record. So.... set
           pointer to first record, then advance and advance.
           You need to check if we still records if we keep advancing

           There are two approches for queries in android


         */

        /**
         * ===================================
         * Approach #1 for query (QUERY)
         * db.query()
         * this method has seven fields
         *
         * ===================================
         */
//        select * from infotable
        //select * from where gender = "female"
        //select * from infotable where dep="HR"

        //create a cursor
        // this means select everything from infotable
        // all of information will go to memory, the cursor will
        // be placed before the first record


        // there are three boolean methods to move the cursor
//        c.moveToFirst();

        //get by fieldno
//        c.getString(FienlNumber);
//        c.getInt(FieldNumber);
//        c.getColumnIndex(ColumnName);

        // go to next record
//        c.moveToNext();


        // check if we there are more records
//        c.isAfterLast();

        //make sure that the cursor is realese when its is done
//        c.close();


        /**
         * db.query(
         * String - databasename
         * Array - array of fields
         * String - where Cluase Fields,
         * Array - array Of Where Clause,
         * Group by - see for more info else just null
         * Having -
         * orderby -
         * limit - give me only first 5
         */
//        Cursor c = db.query("infotable",null,null,null,null,null,null,null);
//        Cursor c = db.query("infotable",null,null,null,null,null,null);
        // EXAMPLE
//        Cursor c = db.query("infotable",null,"name=?",new String[]{"Cathy"},null,null,null,null);

        //we want to sor to sort the items
        // we wnat to order by age desc -> bigtosmall, asc -> small to big

//        Cursor c = db.query("infotable",null,"name=?",new String[]{"Cathy"},null,null,"age ASC",null);

        //in order to retrieve we use a while loop
        //but we can use a for loop
        /*
        for(c.moveToFirst();!c.isAfterLast();c.moveToNext())
        {
            c.getString(c.getColumnCount());
        }
        */

        /**
         * ===================================
         * Approach #1 for query (RAW QUERY)
         * db.rawQuery
         * ===================================
         * only two fields are involved.
         * 1 field is the actual query
         * each question mark relates to one
         * piece of data in the array
         *
         * this is easier
         */

//        Cursor c = db.rawQuery("select * from infotable where name=?", new String[]{"John"});

        // THIS WILL BE IN THE FINAL
        // make select how you would like to run a query

        /*
        * =======================================
        * **************************************
        * REMOVING TABLE
        * **************************************
        * =======================================
        */
        // this will delete table entirely
//        db.execSQL("drop table info");
        // do this instead
//        db.execSQL("drop table if exists info");

        //make sure to close the database
        db.close();


/*****************************************************************************************************************
*
* ======================================
*  HOMEWORK 5 -- TWO WEEKS TO DO HOMEWORK
* ======================================
* we have to have 3 activies
* splash, game, show scores
/*****************************************************************************************************************

Get file US_States from server
1. create a database, call it whatever
2. inside database create a table
    - the table should have 3 fields
    - Field 1 is id
    - Field 2 is State (String)
    - Field 3 is Capital (String)
3. Populate table from this file
    - loop over,read data and add to
    table
4. At the end we should have 50 states

// this file is on hills, how will we
// transfer from hills to our cellphoen
option 1: manually put into sdcard and read
data from sd card.

example terminal commmand:
adb push US_States /data/data/.....

you can open and read from file and put data into the
database table

5. Splash is in charge of
create table,database, populate, ask user name
get the name and start the game.

6. splash is also in charge of
create a table for names and scores
id,name,score
we should be able to have null values for score

////////////////////////////
GAME ACTIVITY
///////////////////////////

The program will generate 5 random capital names
example:
create a random capital, say sacramento
We show to user, is it a state or capital?
two things could happen, if it is a state you loose the round
if it is correct 10 points for the right answer, 0 points for the
wrong answer.

If correct, we ask what is the state for that
capital if correct we add 10 points

---------------

We get Albany, if they type capital they get 10 points
Then if they type new york then they get 10 points

max score is 100

Once they get all the scores we add to table

- update the users score to a number


Slowmotion:
Game starts
- we get new york
- they answer capital
- they get 0
- Round 1 finished
- Round 2 starts
- Program randomly gives new mexico
- is it state or capital
- user answers satename
- they get it correct and get 10 points


------------------------------------
you can put all all states and capitals
in an array

////////////////////////////
Scores ACTIVITY
///////////////////////////
We should sort the results based on scores

select * from stateInfo order by scores limit 10;

we will have
Cursor c = db.rawQuery("select * from stateInfoTable orderby scores limit=?",new String[]{"10"});
Cursor c = db.rawQuery("select * from stateInfoTable orderby DESC limit=?",new String[]{"10"});

3rd activity open database, create cursor, loop over cursor

// ============================
Last homework asignement
will consist of different balloons

hw6
frame layout allows only one item

---------------------------------------------------------
                                                        |
                    linear layout                       |
                                                        |
    ----------------------------------------------|     |
    |                                             |     |
    |                                             |     |
    |               frame layout                  |     |
    |                                             |     |
    |---------------------------------------------|     |
                                                        |
    |----------------------------------------------|    |
    |           Linear layout - horizontal         |    |
    |   ------------------  -------------------    |    |
    |   |  add balloon   |  |  add balloon   |     |    |
    |                                              |    |
    |----------------------------------------------|    |
                                                        |
---------------------------------------------------------

you can also have ardio buttons to select if stat or capital