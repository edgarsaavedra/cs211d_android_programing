/*
 Author: Edgar Saavedra
 Date  : 4/22/15
 Program Name: Scores.java
 Objective: This program lists all users scores
            in descending order from top score
            to lowest score
*/
package com.cs211d.usstates;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class Scores extends Activity {

    //********************************* onCreate()*********************************
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_scores);

        // for testing: output to screen results
        TextView tv = (TextView)findViewById(R.id.listscores);
        tv.setMovementMethod(new ScrollingMovementMethod());
        SQLiteDatabase users;
        users = openOrCreateDatabase("names.db",SQLiteDatabase.CREATE_IF_NECESSARY,null);

        // lets list all of the users
        Cursor c = users.query("names",null,null,new String[]{},null,null,"score DESC",null);
        while(c.moveToNext())
        {
            int ni = c.getColumnIndex("name");
            int si = c.getColumnIndex("score");
            tv.append("\n "+c.getString(si)+" | "+c.getString(ni));
        }

        // make sure to close connection
        c.close();
        users.close();

        //go to splash
        Button btnGame = (Button)findViewById(R.id.backtogame);
        btnGame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                explicitIntent(UsStates.class);
            }
        });
    }

    //********************************* explicitIntent()*********************************
    public void explicitIntent(Class intent)
    {
        try{
            Intent i = new Intent(getApplicationContext(),intent);

            if(i.resolveActivity(getPackageManager()) != null)
            {
                startActivity(i);
            }
        }catch (Exception e)
        {
            toastMessage(e+"");
        }
    }

    //********************************* toastMessage()*********************************
    public void toastMessage(String message)
    {
        Toast t = Toast.makeText(getBaseContext(),message,Toast.LENGTH_SHORT);
        t.show();
    }
}
