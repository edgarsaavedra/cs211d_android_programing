/*
 Author: Edgar Saavedra
 Date  : 4/22/15
 Program Name: UsStates.java
 Objective: This program is a splash screen for our game
            where users login or see scores
*/

package com.cs211d.usstates;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;


public class UsStates extends Activity {

    //the password
    final String pass = "pass";

    //********************************* onCreate()*********************************
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);

        setContentView(R.layout.activity_us_states);

        //create databases and tables
        createStatesDatabase();
        createUsersDatabase();

        // to start the game
        Button btn = (Button) findViewById(R.id.startgame);

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //get entered credentials
                EditText userName = (EditText)findViewById(R.id.userId);
                EditText password = (EditText)findViewById(R.id.password);
                String userNameEntered = userName.getText().toString();
                String passEntered = password.getText().toString();

                //check if the password is valid
                if(  passEntered.compareTo(pass)  == 0)
                {

                    SQLiteDatabase names;
                    names = openOrCreateDatabase("names.db",SQLiteDatabase.CREATE_IF_NECESSARY,null);
                    names.setLockingEnabled(true);
                    names.setVersion(1);
                    Cursor mCursor;
                    mCursor = names.rawQuery("SELECT * FROM names WHERE name=?", new String[]{userNameEntered});

                    boolean exist = (mCursor.getCount() > 0);

                    if (!exist)
                    {

                        ContentValues data = new ContentValues();
                        data.put("name",userNameEntered);
                        data.put("score",0);
                        names.insert("names",null,data);
                    }
                    mCursor.close();
                    names.close();

                    try{
                        Intent i = new Intent(getApplicationContext(),StateGame.class);

                        if(i.resolveActivity(getPackageManager()) != null)
                        {
                            i.putExtra("username_intent",userNameEntered);
                            startActivity(i);
                        }
                    }catch (Exception e)
                    {
                        toastMessage(e+"");
                    }
                }else
                {
                    //let the user know they dont have the
                    //right credentials
                    toastMessage("Incorrect password or username");
                }
            }
        });

        // got to see the scores
        Button btn2 = (Button) findViewById(R.id.gotoscores);

        btn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                explicitIntent(Scores.class);
            }
        });

    }

    //********************************* getStatesAndCapitals()*********************************
    public ArrayList<UsState> getStatesAndCapitals()
    {

        try{
            ArrayList<UsState> statesCapitols = new ArrayList<UsState>();
            Resources r = getApplicationContext().getResources();
            InputStream is = r.openRawResource(R.raw.usstates);
            BufferedReader reader = new BufferedReader(new InputStreamReader(is));

            String line = reader.readLine();
            line = reader.readLine();
            line = reader.readLine();

            String[] words;
            int id = 0;
            while(line != null)
            {
                id++;

                words = line.split("\\s{3,}");
                statesCapitols.add(new UsState(id,words[0],words[1]));
                line = reader.readLine();
            }

            reader.close();
            return statesCapitols;
        } catch (IOException e) {
            toastMessage(e.toString());
            return null;
        }

    }

    //********************************* explicitIntent()*********************************
    public void explicitIntent(Class intent)
    {
        try{
            Intent i = new Intent(getApplicationContext(),intent);

            if(i.resolveActivity(getPackageManager()) != null)
            {
                startActivity(i);
            }
        }catch (Exception e)
        {
            toastMessage(e+"");
        }
    }

    //********************************* toastMessage()*********************************
    public void toastMessage(String message)
    {
        Toast t = Toast.makeText(getBaseContext(),message,Toast.LENGTH_SHORT);
        t.show();
    }

    //********************************* UsState *********************************
    public class UsState
    {
        int id;
        String stateName;
        String capitol;

        public UsState()
        {
           id = 0;
           stateName ="";
           capitol ="";
        }
        public UsState(int i,String sN, String cN)
        {
            id = i;
            stateName = sN;
            capitol = cN;
        }
        public int getid()
        {
            return id;
        }
        public String getStateName()
        {
            return stateName;
        }
        public String getCapitol()
        {
            return capitol;
        }
    }

    //********************************* createStatesDatabase() *********************************
    public void createStatesDatabase()
    {
        SQLiteDatabase db;


        db = openOrCreateDatabase("usstates.db",SQLiteDatabase.CREATE_IF_NECESSARY,null);
        db.setLockingEnabled(true);
        db.setVersion(1);

        String sqlcmd = "CREATE TABLE IF NOT EXISTS states"+
                " ( id INTEGER PRIMARY KEY,"+
                "state TEXT,"+
                "capital TEXT);";

        db.execSQL(sqlcmd);

        // to add data to our database
        ContentValues data = new ContentValues();

        // all of our capitals as read from the file
        ArrayList<UsState> statesCapitols = getStatesAndCapitals();


        // lets add the capitals that dont exist
        Cursor mCursor;
        for (UsState st:statesCapitols)
        {
            mCursor = db.rawQuery("SELECT * FROM states WHERE  state=? AND capital=?", new String[]{st.getStateName(),st.getCapitol()});
            boolean exist = (mCursor.getCount() > 0);

            if (!exist)
            {
                data = new ContentValues();
                data.put("state",st.getStateName());
                data.put("capital",st.getCapitol());
                db.insert("states",null,data);
            }
        }
    }

    //********************************* createUsersDatabase() *********************************
    public void createUsersDatabase()
    {
        SQLiteDatabase db;

        db = openOrCreateDatabase("names.db",SQLiteDatabase.CREATE_IF_NECESSARY,null);
        db.setLockingEnabled(true);
        db.setVersion(1);

        String sqlcmd = "CREATE TABLE IF NOT EXISTS names"+
                " ( id INTEGER PRIMARY KEY,"+
                "name TEXT,"+
                "score INTEGER);";

        db.execSQL(sqlcmd);

        db.close();
    }

    //********************************* showListingOfStates() *********************************
    public void showListingOfStates()
    {

        // for testing: output to screen results
        TextView tv = (TextView)findViewById(R.id.temp);
        tv.setMovementMethod(new ScrollingMovementMethod());

        SQLiteDatabase usstates;
        usstates = openOrCreateDatabase("usstates.db",SQLiteDatabase.CREATE_IF_NECESSARY,null);
        usstates.setLockingEnabled(true);
        usstates.setVersion(1);

        // lets list all of the states
        Cursor c = usstates.query("states",null,null,new String[]{},null,null,"state DESC",null);
        while(c.moveToNext())
        {
            int ii = c.getColumnIndex("id");
            int si = c.getColumnIndex("state");
            int ci = c.getColumnIndex("capital");
            tv.append("\n "+c.getString(ii)+" -- "+c.getString(si)+" "+c.getString(ci));
        }
        c.close();
        // make sure to close connection
        usstates.close();
    }

}
