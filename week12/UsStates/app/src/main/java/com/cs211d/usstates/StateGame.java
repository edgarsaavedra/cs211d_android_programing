/*
 Author: Edgar Saavedra
 Date  : 4/22/15
 Program Name: StateGame.java
 Objective: This program starts a new
            instance of a guessing game
            each round is worth 10 points
            and there are 10 rounds. It
            also logs users scores.
*/
package com.cs211d.usstates;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Random;

public class StateGame extends Activity {

    //********************************* onCreate()*********************************
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_stategame);

        Bundle bundle = getIntent().getExtras();
        String currentusername = bundle.getString("username_intent");

        if(currentusername != "")
        {
            final Game theGame = new Game();
            theGame.startGame(currentusername);

            Button btnMain = (Button)findViewById(R.id.backtomain);
            btnMain.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    theGame.endGame();
                    Intent i = new Intent();
                    setResult(RESULT_OK,i);
                    finish();
                }
            });


            Button toscrores = (Button)findViewById(R.id.toscores);

            toscrores.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    theGame.endGame();
                    explicitIntent(Scores.class);
                }

            });


            Button statebtn = (Button)findViewById(R.id.isstate);

            statebtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    theGame.isCorrect("state");
                }

            });

            Button capbtn = (Button)findViewById(R.id.iscapital);

            capbtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    theGame.isCorrect("capital");
                }

            });
        }


    }

    //********************************* explicitIntent()*********************************
    public void explicitIntent(Class intent)
    {
        try{
            Intent i = new Intent(getApplicationContext(),intent);

            if(i.resolveActivity(getPackageManager()) != null)
            {
                startActivity(i);
            }
        }catch (Exception e)
        {
            toastMessage(e+"");
        }
    }

    //********************************* toastMessage()*********************************
    public void toastMessage(String message)
    {
        Toast t = Toast.makeText(getBaseContext(),message,Toast.LENGTH_SHORT);
        t.show();
    }

    //********************************* Game *********************************
    public class Game
    {
        int currentScore;
        int currentRound;
        int point = 10;
        ArrayList<String> answers;
        ArrayList<StateOrCapital> statesOrCaps;
        String currentPlayerName;
        User user;


        final int TOPSCORE = 100;
        final int MAXROUNDS = 10;

        //********************************* startGame()*********************************
        public void startGame(String username)
        {
            statesOrCaps = new ArrayList<StateOrCapital>();
            answers = new ArrayList<String>();
            currentScore = 0;
            currentRound = 1;

            //get the current user from the last activity
            currentPlayerName = username;

            if(currentPlayerName != "") {
                user = new User(currentPlayerName);
                showAnswer();
            }else{
                endGame();
                explicitIntent(UsStates.class);
            }

        }

        //********************************* endGame()*********************************
        public void endGame()
        {
            statesOrCaps = new ArrayList<StateOrCapital>();
            answers = new ArrayList<String>();
            currentScore = 0;
            currentRound = 0;
            currentPlayerName = "";
            user = null;
        }

        //********************************* getRandomAnswer()*********************************
        private String getRandomAnswer()
        {
            String randomAnswer ="";
            String[] capOrState = {"state","capital"};
            int rnd = new Random().nextInt(capOrState.length);
            String randomChoice = capOrState[rnd];


            SQLiteDatabase usstates;
            usstates = openOrCreateDatabase("usstates.db",SQLiteDatabase.CREATE_IF_NECESSARY,null);
            usstates.setLockingEnabled(true);
            usstates.setVersion(1);


            Cursor cursor = usstates.query("states",new String[]{randomChoice},null, new String[]{},null,null,"RANDOM()","1");

            int columnindex;

            while(cursor.moveToNext())
            {
                columnindex = cursor.getColumnIndex(randomChoice);
                randomAnswer = cursor.getString(columnindex);
            }

            if (answers.size() > 0)
            {
                while (answers.contains(randomAnswer))
                {
                    cursor = usstates.query("states",new String[]{randomChoice},null, new String[]{},null,null,"RANDOM()","1");

                    while(cursor.moveToNext())
                    {
                        columnindex = cursor.getColumnIndex(randomChoice);
                        randomAnswer = cursor.getString(columnindex);
                    }
                }
            }

            if (randomChoice == "state")
            {
                answers.add(randomAnswer);
                StateOrCapital newState = new StateOrCapital("state",randomAnswer);
                statesOrCaps.add(newState);

            }else if(randomChoice == "capital")
            {
                answers.add(randomAnswer);
                StateOrCapital newCap = new StateOrCapital("capital",randomAnswer);
                statesOrCaps.add(newCap);

            }

            cursor.close();
            usstates.close();
            return randomAnswer;
        }

        //********************************* showAnswer()*********************************
        private void showAnswer()
        {
            TextView tv = (TextView) findViewById(R.id.ramdomanswer);
            String answer = getRandomAnswer();

            tv.setText(answer);
        }

        //********************************* isCorrect()*********************************
        public void isCorrect(String choice)
        {
            TextView tv = (TextView)findViewById(R.id.iscorrect);
            tv.clearComposingText();


            StateOrCapital current = statesOrCaps.get(statesOrCaps.size()-1);
            if(choice == current.getStateOrCap())
            {
                tv.setTextColor(Color.BLUE);
                tv.setText("correct! "+current.getName()+" is a "+choice);
                currentScore += 10;

                // update the db of users
                user.updateScore(10);

            }else
            {
                tv.setTextColor(Color.RED);
                tv.setText("wrong! "+current.getName()+" is a "+current.getStateOrCap());
            }
            nextRound();
        }

        //********************************* nextRound()*********************************
        private void nextRound()
        {
            currentRound++;
            if(currentRound <= MAXROUNDS)
            {
                showAnswer();
            }else
            {
                explicitIntent(Scores.class);
            }
        }

    }

    //********************************* StateOrCapital *********************************
    public class StateOrCapital
    {
      String stateOrCap;
      String name;

      //********************************* StateOrCapital() *********************************
      public StateOrCapital()
      {
        stateOrCap = "";
        name = "";
      }

      //********************************* StateOrCapital() *********************************
      public StateOrCapital(String sOrC, String n)
      {
          stateOrCap = sOrC;
          name = n;
      }

      //********************************* getStateOrCap() *********************************
      public String getStateOrCap()
      {
          return stateOrCap;
      }

      //********************************* getName() *********************************
      public String getName()
      {
          return name;
      }
    }

    //********************************* User *********************************
    public class User {

        private String name;
        private Integer score;

        //********************************* User() *********************************
        public User(String n)
        {
            setName(n);
            score = getScore();
        }

        //********************************* setName() *********************************
        private void setName(String n)
        {
            name = n;
        }

        //********************************* updateScore() *********************************
        public void updateScore(int n)
        {
            if(name != "")
            {
                score = getScore();

                ContentValues cv2 = new ContentValues();
                cv2.put("score",score+n);

                SQLiteDatabase db;
                db = openOrCreateDatabase("names.db",SQLiteDatabase.CREATE_IF_NECESSARY,null);
                db.setLockingEnabled(true);
                db.setVersion(1);

                db.update("names", cv2, "name=?", new String[]{name});

                db.close();

                score = getScore();

            }

        }

        //********************************* getScore() *********************************
        private Integer getScore()
        {
            Integer theScore = 0;

            SQLiteDatabase db;
            db = openOrCreateDatabase("names.db",SQLiteDatabase.CREATE_IF_NECESSARY,null);
            db.setLockingEnabled(true);
            db.setVersion(1);

            Cursor cursor = db.query("names",new String[]{"score"},"name=?", new String[]{name},null,null,null,"1");
            int columnindex;
            while(cursor.moveToNext())
            {
                columnindex = cursor.getColumnIndex("score");
                theScore = (int)cursor.getInt(columnindex);
            }
            cursor.close();
            db.close();

            return theScore;
        }

    }

}
