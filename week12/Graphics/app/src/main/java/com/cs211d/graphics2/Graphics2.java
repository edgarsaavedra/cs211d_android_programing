package com.cs211d.graphics2;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
//import android.R;


import android.graphics.*;
import android.view.View;
import android.view.*;



public class Graphics2 extends Activity {
    DemoView dv;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.demoview);

       dv = new DemoView(this);
       dv.setBackgroundColor(Color.RED);
       setContentView(dv);


    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_graphics, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public class DemoView extends View
    {
        Paint p = new Paint();
        //or at time of creating object of paint
        // you can set anti alias
        //QUESTION IN FINAL
        //new Paint(Paint.ANTI_ALIAS_FLAG);

        //to draw polygon
        Path pt = new Path();
        int x[] = {100,200,400,600,300,100};
        int y[] = {0,10,20,30,40,0};

        // we can create color
        // r,g,b
        int col = Color.rgb(0,60,20);

        // to draw lots of lines
        // this is for 2 lines
        // IN FINAL EXAM
        float ar[]={200,100,120,80,140,50,110,200};

        public DemoView(Context con)
        {
            //call constructor of view
            super(con);
            // set back ground color
//            p.setColor(Color.BLACK);
            p.setColor(Color.BLUE);

            // for characters you need to set the width
            p.setStyle(Paint.Style.STROKE);

            // text size, 30 pixels
            p.setTextSize(30);



            // we can set color
//            p.setColor(Color.rgb(255,255,0));
//

            //we want anti aliasing
            p.setAntiAlias(true);

            //these two lines will help set a
            //thicker line
//            p.setStyle(Paint.Style.STROKE);
//            p.setStrokeWidth(100);

            // to fill the circle
//            p.setStyle(Paint.Style.FILL);
            // to use if you have colors in xml file you
            // can call by id in setColor
            //or we can use html style
//            p.setColor(Color.parseColor("#FF0000"));

        }

        @Override
        public void onDraw(Canvas c)
        {

            c.drawLines(ar,p);
            // for transparancy
            c.drawColor(Color.TRANSPARENT);

            // if it doesnt work
//            c.drawColor(Color.TRANSPARENT,PorterDuff.Mode.CLEAR);


            // to draw a line you need two points
            // you need to also pass the paint
            // paint is carrying color,style,thickness
            c.drawLine(0,0,400,400,p);
            //lets make another line
            c.drawLine(400,0,0,400,p);

            // rectangle
            c.drawRect(100,5,200,30,p);

            // draw rect with floating point
            RectF rf = new RectF(0,0,getWidth(),getHeight());
            c.drawOval(rf,p);

            // draw circle
            c.drawCircle(89,30,70,p);

            // empty oval
            //imagin oval is inside rectangle
            c.drawOval(80,20,40,80,p);

            // you have own methods drwa polygon etc.
            pt.moveTo(x[0],y[0]);
            //ask system to draw line from point to point
//            pt.lineTo(x[1],y[1]);

            for (int i = 1; i<x.length; i++)
            {
                //ask system to draw line from point to point
                pt.lineTo(x[i],y[i]);
            }
            // finally use canvas to draw
            c.drawPath(pt,p);

            // draw some text
            // x an y of begining of text
            // IMPORTANT: p carries all properties
            // begining is left upper coner of character
            c.drawText("Hello world!",75,80,p);
        }
    }
}
