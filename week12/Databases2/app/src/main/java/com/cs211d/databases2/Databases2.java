package com.cs211d.databases2;

import android.app.Activity;
import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;


public class Databases2 extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_databases2);

        SQLiteDatabase db;

        //create the database
        // this will create the database in: /data/data/com.cs211d/databases/mydb.db
        db = openOrCreateDatabase("mydb.db",SQLiteDatabase.CREATE_IF_NECESSARY,null);

        //set up locking and version number
        // prevents database corruption
        db.setLockingEnabled(true);
        //start from 1 and increment by 1
        db.setVersion(1);

        // the sql command to execute
        // execsql is a sql command that does
        //not return anything

        //drop a table
//        String sqlcmd = "drop table in foo;";

        //sql command to add a table
//        String sqlcmd = "create table infotable (_id integer"+"primary";

//        String sqlcmd = "CREATE TABLE infotable"+
//                " ( id INTEGER PRIMARY KEY,"+
//                "name TEXT,"+
//                "age INTEGER,"+
//                "dep TEXT);";

        // create the table and execute our command
//        db.execSQL(sqlcmd);

        // ========= normally in a loop

        //after create a table we use the insert command to
        //populate the table, we need to  create an object
        ContentValues cv = new ContentValues();
        cv.put("name","Edgar");
        cv.put("age",25);
        cv.put("dep","dev");

        // we insert the data this tells you
        // how many records are processed
        // this should be in a throw
        long recNum = db.insert("infotable",null,cv);

        // ========= normally in a loop

        /*
        * =======================================
        * **************************************
        * UPDATE WITH  WHERE CLAUSE
        * **************************************
        * =======================================
        */

        //1 create content value

        ContentValues cv2 = new ContentValues();
        cv2.put("age",30);

        /*
        * =======================================
        * **************************************
        * UPDATE WITH MULTIPLE WHERE CLAUSE
        * **************************************
        * =======================================
        *
        * We want to be more specific and require another where
        * clause
        *
        * db.update("infotable", cv, "name=? and dep=?", new String[]{"John","HR"});
        * */
        db.update("infotable", cv, "name=? and dep=?", new String[]{"Edgar","dev"});

        /*
        *
        * =======================================
        * **************************************
        * DELETING RECORDS
        * **************************************
        * =======================================
        *
        * To delete the entire table in database
        * ===========================================
        * delete info;
        *
        * If we want to delete in SQL we run this query
        * ============================================
        *
        * delete info where dept="ENG";
        *
        * THis is how we delete record in android
        * delete method:
        * delet(String tablename, String whereClause, Array whereArgument)
        * exmple:
        * */
        //delete record with david as a name in it


//         db.delete("infotable","name=?",new String[]{"David"});


        /*
        * =======================================
        * **************************************
        * RUNNIG QUERY
        * **************************************
        * =======================================
        */

        /*
           all of the data that is part of a query
           will be collected in memory and will be
           saved in memory. A query returns a value

           there is a pointer that points to the line
           above the very first record. This pointer is
           called a cursor. Cursor is a pointer that highlights
           where the data is residing in memory. You will
           be moving the pointer to the next record

           We will retrieve the first record, after retrieving
           we will advance to the next record. So.... set
           pointer to first record, then advance and advance.
           You need to check if we still records if we keep advancing

           There are two approches for queries in android


         */

        /**
         * ===================================
         * Approach #1 for query (QUERY)
         * db.query()
         * this method has seven fields
         *
         * ===================================
         */
//        select * from infotable
        //select * from where gender = "female"
        //select * from infotable where dep="HR"

        //create a cursor
        // this means select everything from infotable
        // all of information will go to memory, the cursor will
        // be placed before the first record


        // there are three boolean methods to move the cursor
//        c.moveToFirst();

        //get by fieldno
//        c.getString(FienlNumber);
//        c.getInt(FieldNumber);
//        c.getColumnIndex(ColumnName);

        // go to next record
//        c.moveToNext();


        // check if we there are more records
//        c.isAfterLast();

        //make sure that the cursor is realese when its is done
//        c.close();


        /**
         * db.query(
         * String - databasename
         * Array - array of fields
         * String - where Cluase Fields,
         * Array - array Of Where Clause,
         * Group by - see for more info else just null
         * Having -
         * orderby -
         * limit - give me only first 5
         */
//        Cursor c = db.query("infotable",null,null,null,null,null,null,null);
//        Cursor c = db.query("infotable",null,null,null,null,null,null);
        // EXAMPLE
//        Cursor c = db.query("infotable",null,"name=?",new String[]{"Cathy"},null,null,null,null);

        //we want to sor to sort the items
        // we wnat to order by age desc -> bigtosmall, asc -> small to big

//        Cursor c = db.query("infotable",null,"name=?",new String[]{"Cathy"},null,null,"age ASC",null);

        //in order to retrieve we use a while loop
        //but we can use a for loop
        /*
        for(c.moveToFirst();!c.isAfterLast();c.moveToNext())
        {
            c.getString(c.getColumnCount());
        }
        */

        /**
         * ===================================
         * Approach #1 for query (RAW QUERY)
         * db.rawQuery
         * ===================================
         * only two fields are involved.
         * 1 field is the actual query
         * each question mark relates to one
         * piece of data in the array
         *
         * this is easier
         */

//        Cursor c = db.rawQuery("select * from infotable where name=?", new String[]{"John"});

        // THIS WILL BE IN THE FINAL
        // make select how you would like to run a query

        /*
        * =======================================
        * **************************************
        * REMOVING TABLE
        * **************************************
        * =======================================
        */
        // this will delete table entirely
//        db.execSQL("drop table info");
        // do this instead
//        db.execSQL("drop table if exists info");

        //make sure to close the database
        db.close();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_databases2, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
