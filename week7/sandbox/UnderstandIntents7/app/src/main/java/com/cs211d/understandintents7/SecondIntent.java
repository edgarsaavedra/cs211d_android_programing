package com.cs211d.understandintents7;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

//this view responsible to tell
// taht name is found and return

public class SecondIntent extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second_intent);

        Button btn = (Button) findViewById(R.id.btnClick1);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                this i a message that you are sendin
//                back to the caller, this specialy important
//                if we are calling back and forth
//                in reality we would do something and
//                if the resut is ok then we
//                setResult(RESULT_OK);
//                finish();

                /*
                    create an intent
                 */
//                create intent to send data back
                Intent i = new Intent();
//                prep data to send
                i.putExtra("name","mariana");
//                now send it back and say if ok or not
                setResult(RESULT_OK,i);
                finish();
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_understand_intents7, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();



        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings)
//        if (item.getItemId() == 1)
        {

//            Intent i = new Intent();
            //intent creattion done correctly
            //if everything is ok, then run the intent
            //google has requested that all devlopers
            //check that ll is ok

            Intent i = new Intent(getApplicationContext(),UnderstandIntents7.class);
            if(i.resolveActivity(getPackageManager()) != null)
            {
                startActivity(i);
            }

        }else{
            return super.onOptionsItemSelected(item);
        }
        return true;
    }
}
