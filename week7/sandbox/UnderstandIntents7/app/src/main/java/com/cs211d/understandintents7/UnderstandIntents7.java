package com.cs211d.understandintents7;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.AlarmClock;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import org.w3c.dom.Text;

import static android.provider.AlarmClock.*;


public class UnderstandIntents7 extends Activity {
    final int MY_ID = 1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_understand_intents7);

        Button btn = (Button) findViewById(R.id.btnClick);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(),SecondIntent.class);
    //            startActivity(i);

    //            passing data to activity
                startActivityForResult(i,MY_ID);
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
//        check if request code is same as yours
        if(requestCode == MY_ID && resultCode == RESULT_OK){
//            get data with bundle
            Bundle bun = getIntent().getExtras();
            if(bun != null)
            {
                String name = bun.getString("name");
                TextView tv = (TextView) findViewById(R.id.name_from_callback);

                tv.setText("The name is -- "+name);
            }

        }
//        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_understand_intents7, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
//        if (id == R.id.action_settings)
        if (id == R.id.action_settings) {

            //intent creattion done correctly
            //if everything is ok, then run the intent
            //google has requested that all devlopers
            //check that ll is ok
//            int id = item.getItemId();


//            startActivity(i);

            explicitIntent(SecondIntent.class);
//            createAlarm("alarm1",1,20);
//            callWebBrowser();
//            dial()
//            call("4153927083");

//            showMap(20.9,0.0);
//            showMap(38.8891,-77.0492);
        }else{
            return super.onOptionsItemSelected(item);
        }
        return true;
    }

    /**
     * You have to give android premission to
     * run this action
     * @param msg - the message that should display
     */
    public void createAlarm(String msg, int hr, int min){
//        create intent
//        every intent is created with action and data
//        creating intent and createing data in one one line
//        any data provide in intet should be given as a key value
//        because this is implicit then it will give a list of programs that match
//        we dont know the name
        Intent i = new Intent(ACTION_SET_ALARM)
                .putExtra(EXTRA_MESSAGE,msg)
                .putExtra(EXTRA_HOUR, hr)
                .putExtra(EXTRA_MINUTES, min);
        if(i.resolveActivity(getPackageManager())!= null)
        {
            startActivity(i);
        }
    }

    public void callWebBrowser(){
        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.google.com")));
    }
    public void dial()
    {
        startActivity(new Intent(Intent.ACTION_DIAL));
    }
    public void explicitIntent(Class intent)
    {
        Intent i = new Intent(getApplicationContext(),intent);

        if(i.resolveActivity(getPackageManager()) != null)
        {
            startActivity(i);
        }
    }
//    calls somebody on cell phone
    public void call(String phoneNumber)
    {
        startActivity(new Intent(Intent.ACTION_CALL,Uri.parse("tel:"+phoneNumber)));
    }

    public void showMap(double lat,double lon)
    {
        startActivity(new Intent(Intent.ACTION_VIEW,Uri.parse("geo:"+lat+","+lon)));
    }
}
