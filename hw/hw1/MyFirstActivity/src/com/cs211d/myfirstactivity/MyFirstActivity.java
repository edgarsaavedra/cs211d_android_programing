//  Author: Edgar Saavedra
//  Date  : 02/05/15
//  Program Name: MyFirstActivity.java
//  Objective: This program shows basic android functionality 
//             through the creation of text widgits and use
//             of basic styling.
//****************************************************************

package com.cs211d.myfirstactivity;

import android.app.Activity;
import android.os.Bundle;


public class MyFirstActivity extends Activity
{
    @Override
    //****************************onCreate()****************************
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.myfirstactivity);   
    }
}
