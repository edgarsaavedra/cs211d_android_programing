/*

 Author: Edgar Saavedra
 Date  : 03/19/12
 Program Name: Conversion.java
 Objective: This program demonstrates 
    the use of a login activity
*/

package com.cs211d.conversion;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.Properties;


public class Conversion extends Activity {

    final String user = "edgar";
    final String pass = "tester";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_conversion);

        Button btn = (Button) findViewById(R.id.conversionbutton);

        btn.setOnClickListener(new View.OnClickListener() {


            @Override
            public void onClick(View v) {

                EditText userName = (EditText)findViewById(R.id.userId);
                EditText password = (EditText)findViewById(R.id.password);
                String userNameEntered = userName.getText().toString();
                String passEntered = password.getText().toString();

                Properties prop = new Properties();

                if( userNameEntered.compareTo(user)  == 0
                   &&  passEntered.compareTo(pass)  == 0)
                {
                    explicitIntent(FahrenheitToCelsius.class);
                }else
                {
                    toastMessage("Incorrect password or username");
                }

            }
        });
    }

    public void explicitIntent(Class intent)
    {
        try{
            Intent i = new Intent(getApplicationContext(),intent);

            if(i.resolveActivity(getPackageManager()) != null)
            {
                startActivity(i);
            }
        }catch (Exception e)
        {
            toastMessage(e+"");
        }
    }

    public  void toastMessage(String message)
    {
        Toast t = Toast.makeText(getBaseContext(),message,Toast.LENGTH_SHORT);
        t.show();
    }

}
