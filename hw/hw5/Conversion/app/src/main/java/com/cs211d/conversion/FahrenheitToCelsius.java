/*

 Author: Edgar Saavedra
 Date  : 03/19/12
 Program Name: FahrenheitToCelsius.java
 Objective: This program demonstrates the use of
            on click events in conjuction
            with textfields
*/

package com.cs211d.conversion;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.Toast;


public class FahrenheitToCelsius extends Activity
{

    /** Called when the activity is first created. */
    @Override
    //********************************* onCreate()*********************************
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);

        setContentView(R.layout.fahrenheittocelsius);

        //Resources res =  getResources();
        EditText et = (EditText)findViewById(R.id.convertField);
        final RadioGroup radioGroup = (RadioGroup) findViewById(R.id.group1);


        //main code that excutes either conversion
        et.setOnKeyListener(new View.OnKeyListener(){
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                int radioButtonId;
                radioButtonId = radioGroup.getCheckedRadioButtonId();

                View checkRadioButton;
                checkRadioButton = radioGroup.findViewById(radioButtonId);

                int finalButtonId = radioGroup.indexOfChild(checkRadioButton);

                //cel to far
                if(finalButtonId == 0)
                {
                    celToFar(checkRadioButton);
                    return false;
                }
                // far to cell
                if(finalButtonId == 1)
                {
                    celToFar(checkRadioButton);
                    return false;
                }
                return false;
            }
        });

        Button btnMain = (Button)findViewById(R.id.backtomain);
        btnMain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent();
                setResult(RESULT_OK,i);
                finish();
            }
        });

        //if we want to see what users have
        //used this program
        Button btnListNames = (Button)findViewById(R.id.tonames);
        btnListNames.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                explicitIntent(ListNames.class);
            }
        });

    }

    //********************************* celToFar()*********************************
    public void celToFar(View v)
    {
        EditText et = (EditText)findViewById(R.id.convertField);
        EditText converted = (EditText)findViewById(R.id.convertedField);

        String enteredNumber = et.getText().toString();
        enteredNumber = enteredNumber.replace(" ", "");
        enteredNumber.trim();
        int myEditNum;

        if(!enteredNumber.matches(""))
        {
            try {
                //temperature in degrees
                // Fahrenheit = (temperature in degrees Celsius • 1.8) + 32
                myEditNum = Integer.parseInt(enteredNumber);
                double fahrenheit;
                fahrenheit = (double)(myEditNum * 1.8) + 32;
                converted.setText(fahrenheit+"");
            }catch (Exception e){

            }
        }
    }

    //********************************* farToCel()*********************************
    public void farToCel(View v)
    {
        //temperature in degrees
        // Celsius = (temperature in degrees Fahrenheit – 32) / 1.8
        EditText et = (EditText)findViewById(R.id.convertField);
        EditText converted = (EditText)findViewById(R.id.convertedField);

        String enteredNumber = et.getText().toString();
        enteredNumber = enteredNumber.replace(" ", "");
        enteredNumber.trim();
        int myEditNum;

        if(!enteredNumber.matches(""))
        {
            try {
                //temperature in degrees
                // Fahrenheit = (temperature in degrees Celsius • 1.8) + 32
                myEditNum = Integer.parseInt(enteredNumber);
                double cel;
                cel = (double)(myEditNum - 32) / 1.8;
                converted.setText(cel+"");
            }catch (Exception e){

            }
        }
    }

    //********************************* explicitIntent()*********************************
    public void explicitIntent(Class intent)
    {
        try{
            Intent i = new Intent(getApplicationContext(),intent);

            if(i.resolveActivity(getPackageManager()) != null)
            {
                startActivity(i);
            }
        }catch (Exception e)
        {
            toastMessage(e+"");
        }
    }

    //********************************* toastMessage()*********************************
    public  void toastMessage(String message)
    {
        Toast t = Toast.makeText(getBaseContext(),message,Toast.LENGTH_SHORT);
        t.show();
    }

}