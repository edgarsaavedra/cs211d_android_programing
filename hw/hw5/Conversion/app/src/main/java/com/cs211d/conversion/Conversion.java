/*

 Author: Edgar Saavedra
 Date  : 03/19/12
 Program Name: Conversion.java
 Objective: This program demonstrates 
    the use of a login activity
*/

package com.cs211d.conversion;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


public class Conversion extends Activity {

    //the password
    final String pass = "pass";

    //********************************* onCreate()*********************************
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_conversion);


        Button btn = (Button) findViewById(R.id.conversionbutton);

        //when people want to go to convert
        btn.setOnClickListener(new View.OnClickListener() {


            @Override
            public void onClick(View v) {

                //get entered credentials
                EditText userName = (EditText)findViewById(R.id.userId);
                EditText password = (EditText)findViewById(R.id.password);
                String userNameEntered = userName.getText().toString();
                String passEntered = password.getText().toString();

                //check if the password is valid
                if(  passEntered.compareTo(pass)  == 0)
                {
                    //we want to save the user name
                    SharedPreferences sp = getSharedPreferences("names",0);


                    SharedPreferences.Editor ed = sp.edit();

                    //add the user names to our preferences file
                    ed.putString(userNameEntered,userNameEntered);

                    // use the commit method to actually save
                    ed.commit();

                    //go to the conversion
                    explicitIntent(FahrenheitToCelsius.class);
                }else
                {
                    //let the user know they dont have the
                    //right credentials
                    toastMessage("Incorrect password or username");
                }

            }
        });
    }

    //********************************* explicitIntent()*********************************
    public void explicitIntent(Class intent)
    {
        try{
            Intent i = new Intent(getApplicationContext(),intent);

            if(i.resolveActivity(getPackageManager()) != null)
            {
                startActivity(i);
            }
        }catch (Exception e)
        {
            toastMessage(e+"");
        }
    }

    //********************************* toastMessage()*********************************
    public void toastMessage(String message)
    {
        Toast t = Toast.makeText(getBaseContext(),message,Toast.LENGTH_SHORT);
        t.show();
    }

}
