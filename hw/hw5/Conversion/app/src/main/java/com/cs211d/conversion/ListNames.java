/*

 Author: Edgar Saavedra
 Date  : 03/19/12
 Program Name: ListNames.java
 Objective: This programs shows the use of
            retrieval of shared preferences
            data to list users
*/

package com.cs211d.conversion;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;



public class ListNames extends Activity {

    //********************************* onCreate()*********************************
    public void  onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.listnames);

        //get the names prefs
        SharedPreferences sp = getSharedPreferences("names",0);

        SharedPreferences.Editor ed = sp.edit();

        //get all of the keys
        Map<String,?> keys = sp.getAll();

        //we will update to list
        TextView tv = (TextView) findViewById(R.id.listnames);

        //array list are easier to sort
        List<String> unsortList = new ArrayList<String>();

        for(Map.Entry<String,?> entry : keys.entrySet()){
            unsortList.add(entry.getValue().toString());
        }

        //helper method to easily
        //sort
        Collections.sort(unsortList);

        //append to view
        for(String name : unsortList)
        {
            tv.append("\n"+name);
        }

        //go back to the conversion view
        Button btnMain = (Button)findViewById(R.id.backtoconversion);
        btnMain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent();
                setResult(RESULT_OK,i);
                finish();
            }
        });
    }

}
