/*

 Author: Edgar Saavedra
 Date  : 02/26/11
 Program Name: FahrenheitToCelsius.java
 Objective: This program demonstrates the use of
            on click events in conjuction
            with textfields
*/

package com.cs211d.fahrenheittocelsius;

import android.app.Activity;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.Toast;

import java.security.acl.Group;

public class FahrenheitToCelsius extends Activity
{

    /** Called when the activity is first created. */
    @Override
    //********************************* onCreate()*********************************
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);

        setContentView(R.layout.main);

        //Resources res =  getResources();
        EditText et = (EditText)findViewById(R.id.convertField);
        final RadioGroup radioGroup = (RadioGroup) findViewById(R.id.group1);


        et.setOnKeyListener(new View.OnKeyListener(){
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                int radioButtonId;
                radioButtonId = radioGroup.getCheckedRadioButtonId();

                View checkRadioButton;
                checkRadioButton = radioGroup.findViewById(radioButtonId);

                int finalButtonId = radioGroup.indexOfChild(checkRadioButton);


                if(finalButtonId == 0) //cel to far
                {
                   celToFar(checkRadioButton);
                   return false;
                }
                if(finalButtonId == 1) // far to cell
                {
                   celToFar(checkRadioButton);
                   return false;
                }
                return false;
            }
        });
    }

    //********************************* celToFar()*********************************
    public void celToFar(View v)
    {
        EditText et = (EditText)findViewById(R.id.convertField);
        EditText converted = (EditText)findViewById(R.id.convertedField);

        String enteredNumber = et.getText().toString();
        enteredNumber = enteredNumber.replace(" ", "");
        enteredNumber.trim();
        int myEditNum;

        if(!enteredNumber.matches(""))
        {
            try {
                //temperature in degrees
                // Fahrenheit = (temperature in degrees Celsius • 1.8) + 32
                myEditNum = Integer.parseInt(enteredNumber);
                double fahrenheit;
                fahrenheit = (double)(myEditNum * 1.8) + 32;
                converted.setText(fahrenheit+"");
            }catch (Exception e){

            }
        }
    }

    //********************************* farToCel()*********************************
    public void farToCel(View v)
    {
        //temperature in degrees
        // Celsius = (temperature in degrees Fahrenheit – 32) / 1.8
        EditText et = (EditText)findViewById(R.id.convertField);
        EditText converted = (EditText)findViewById(R.id.convertedField);

        String enteredNumber = et.getText().toString();
        enteredNumber = enteredNumber.replace(" ", "");
        enteredNumber.trim();
        int myEditNum;

        if(!enteredNumber.matches(""))
        {
            try {
                //temperature in degrees
                // Fahrenheit = (temperature in degrees Celsius • 1.8) + 32
                myEditNum = Integer.parseInt(enteredNumber);
                double cel;
                cel = (double)(myEditNum - 32) / 1.8;
                converted.setText(cel+"");
            }catch (Exception e){

            }
        }
    }

}