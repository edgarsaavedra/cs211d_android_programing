/*

 Author: Edgar Saavedra
 Date  : 02/12/11
 Program Name: ButtonFruits.java
 Objective: This program demonstrates the use of widget buttons
            mixed resource types and onclick events.
*/

package com.cs211d.buttonfruits;

import android.app.Activity;
import java.util.Date;
import java.util.Random;

import android.content.res.Resources;
import android.graphics.Color;
import android.hardware.Camera;
import android.os.Bundle;
import android.util.Size;
import android.view.View;
import android.view.Window;
import android.widget.Button;

public class ButtonFruits extends Activity
{
    /** Called when the activity is first created. */
    @Override
    //********************************* onCreate()*********************************
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);

        setContentView(R.layout.main);
    }

    //*********************************doit()*********************************
    public void doit(View v)
    {
        switch (v.getId())
        {
            case R.id.mybtn:
                update(v);
             break;
        }
    }

    //*********************************update()*********************************
    private void update(View v)
    {
        Random num = new Random();
        Resources r = getResources();
        Button thebtn = (Button) v;
        String strArr[] = r.getStringArray(R.array.fruits_bug);
        String[] colorArr = r.getStringArray(R.array.color_buff);
        int current_fruit = num.nextInt(strArr.length)+0;

        //update button text
        thebtn.setText("Push Me: \n"+strArr[current_fruit]);

        //update color
        thebtn.setTextColor(Color.parseColor(colorArr[current_fruit]));
    }


}
