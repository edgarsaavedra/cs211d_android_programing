package com.cs211d.sandbox14;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

/**
 * Created by edgar on 4/30/15.
 */
public class ExtendsListAct extends ListActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        String forArrayAdapter[] = {"apple","mango","ft1","ft2","ft3"};
        ArrayAdapter<String> aa = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,forArrayAdapter);
        getListView().setChoiceMode(ListView.CHOICE_MODE_NONE);
        setListAdapter(aa);
    }
    @Override
    public void onListItemClick(ListView lv, View v, int position, long id)
    {
        Intent i = new Intent(getApplicationContext(),Sandbox14.class);
        startActivity(i);
    }
}
