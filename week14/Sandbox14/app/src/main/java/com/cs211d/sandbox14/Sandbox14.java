package com.cs211d.sandbox14;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;


public class Sandbox14 extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sandbox14);







//        final TextView tv = (TextView) findViewById(R.id.customtext);
//        final ListView lv = (ListView) findViewById(R.id.mylist);
//
//        //catch event on list item
//        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            // <?> any kind of data
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                TextView tv = (TextView) findViewById(R.id.customtext);
//                String text = lv.getItemAtPosition(position).toString();
//                tv.setText(text);
//            }
//        });

        // *********************************************
        //
        // use array adapter,
        // no need to for
        //
        // *********************************************

//        String forArrayAdapter[] = {"apple","mango","ft1","ft2","ft3"};
//        ArrayAdapter<String> aa = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,forArrayAdapter);
//
//
//        final ListView lv2 = (ListView) findViewById(R.id.mylist2);
//
//        lv2.setAdapter(aa);
//
//        lv2.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                TextView tv = (TextView) findViewById(R.id.customtext);
//                String text = lv2.getItemAtPosition(position).toString();
//                tv.setText(text);
//
//                Intent i = new Intent(getApplicationContext(),ExtendsListAct.class);
//                startActivity(i);
//
//            }
//        });

//        createUsersDatabase();
//        ContentValues data = new ContentValues();
//        data.put("name","edgar");
//        data.put("name","edgar2");
//        data.put("name","nm3");
//        data.put("name","nm4");
//        data.put("name","nm5");
//        data.put("name","nm6");
//        data.put("name", "nm7");
//
        SQLiteDatabase db;
        db = openOrCreateDatabase("names.db",SQLiteDatabase.CREATE_IF_NECESSARY,null);
        db.setLockingEnabled(true);
        db.setVersion(1);
//
//
//        db.insert("names", null, data);
//
//        // cursor adapter
        Cursor mCursor = db.rawQuery("SELECT * FROM names", null,null);
//
//        SimpleCursorAdapter sca = new SimpleCursorAdapter(this,android.R.layout.simple_list_item_single_choice, mCursor ,new String[]{"name"},new int[]{android.R.id.text1});
//        ListView lv3 = (ListView) findViewById(R.id.mylist3);
//        lv3.setAdapter(sca);

        db.close();


    }

    public void createUsersDatabase()
    {
        SQLiteDatabase db;

        db = openOrCreateDatabase("names.db",SQLiteDatabase.CREATE_IF_NECESSARY,null);
        db.setLockingEnabled(true);
        db.setVersion(1);

        String sqlcmd = "CREATE TABLE IF NOT EXISTS names"+
                " ( id INTEGER PRIMARY KEY,"+
                "name TEXT"+
                ");";

        db.execSQL(sqlcmd);

        db.close();
    }




//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.menu_sandbox14, menu);
//        return true;
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        // Handle action bar item clicks here. The action bar will
//        // automatically handle clicks on the Home/Up button, so long
//        // as you specify a parent activity in AndroidManifest.xml.
//        int id = item.getItemId();
//
//        //noinspection SimplifiableIfStatement
//        if (id == R.id.action_settings) {
//            return true;
//        }
//
//        return super.onOptionsItemSelected(item);
//    }
}
