/**
 * This program
 * that print a sequence of a number
 * the async task will generate the
 * number representing taks
 */

package com.cs211d.sandbox;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.SystemClock;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

/**
 * this week we are dealing with
 * varible arguments
 *
 * look in to class ProgressBar in android
 * : http://developer.android.com/reference/android/widget/ProgressBar.html
 */

public class Sandbox extends Activity {
    TextView tv;
    public String names[] = {"jon","alex","david"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sandbox);

        tv = (TextView) findViewById(R.id.progress);


        // now start doint the
        // execute(1) means start sequence at 1
        // this could also be the name of pictures
        // we want to download
        DemoAsyncTask test =  new DemoAsyncTask();
        test.execute(1);

//        new ReverseNameTask().execute(1,4,6);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_sandbox, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /*
        varArg - anonymouse parameter

        the varArg is represented as an array
        it is very usefull when you want to allow
        multiple types of arguments instead of
        function over loading

        vararg(variable argument);
    */

    public int submit( int ... num)
    {
        int sum = 0;

        for(int n : num )
        {
            sum += n;
        }
        return sum;
    }

    /**
     * This class only works 3 types of data, this class
     * only handles 3 types of data
     * the first one dictates what type of data is as input
     * Data comes through your class from execute
     * Middle data provide progress information
     * Last Data type is for result
     */
    public class DemoAsyncTask extends AsyncTask <Integer, Integer,Void>
    {

        // job of background task should be here
        @Override
        protected Void doInBackground(Integer... params) {

            SystemClock.sleep(3000);
            for(int i = params[0]; i <= 10; i++)
            {

                // notify the user where we are
                //publishProgress is a method of async task and will call
                // the progress during, this method is optional
                publishProgress(i);

                // this method in android makes a delay
                // if this was being downloaded we would need to delay
                // the network
                // networking in android CHAPTER 4 http protocol
                SystemClock.sleep(200);
            }


            return null;
        }

        @Override
        protected void onPostExecute(Void params)
        {
            tv.setText("completed");
        }


        @Override
        protected void onPreExecute()
        {
            tv.setText("started");
//            updatetext("Sequence Numbers Begins");
        }

        // during
        @Override
        protected void onProgressUpdate(Integer ... values)
        {
            updatetext(values[0].toString());
        }

    }

    public class ReverseNameTask extends AsyncTask<Integer,String,String>
    {
        @Override
        protected String doInBackground(Integer... params) {

            String str = "";

            for (int i = 0; i < names.length ; i++)
            {
                str += names[i]+":";

                if(i == params[0] || i == params[1] || i == params[2])
                {
                    publishProgress(reverse(names[i]));
                    SystemClock.sleep(500);
                }
            }

            return str;
        }

        @Override
        protected void onProgressUpdate(String ... values)
        {
            //do what ever
        }

        private String reverse(String name)
        {
            return  "";
        }

        // whatever is returns form doinbackground
        // is what is pass as the param so only 1 item
        @Override
        protected  void onPostExecute(String res)
        {
            // nothing comes back
            tv.setText(res);
        }

    }

    public void updatetext(String s)
    {

        tv.setText(s);
    }


}
