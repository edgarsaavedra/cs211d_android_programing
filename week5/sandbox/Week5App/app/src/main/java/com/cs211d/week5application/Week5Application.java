package com.cs211d.week5application;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Toast;

public class Week5Application extends Activity
{
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        String x = "test";
        final String TAG="hw3" ;
        Log.d(TAG, "x=" + x);

        //Toast example

        //create toaster object
        Toast t = Toast.makeText(getBaseContext(),"Battery low!!",Toast.LENGTH_SHORT);
        t.show();

        final EditText et = (EditText)findViewById(R.id.tfield);
//        et.setText("first line is here \n"+
//                   "second line is here \n"+
//                   "last line is here");

        //events for input fields
        //anonymous inner class is the best way to handle
        //and event
        et.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                //if any key pressed and that key is enter
                if((event.getAction()==KeyEvent.ACTION_DOWN) && (keyCode==KeyEvent.KEYCODE_ENTER))
                {
//                    mkToast("key code: "+keyCode);
                    //gettext() returns object
//                    if(et.getText().toString() == "89")
//                    {
//                        mkToast("89");
//                        //im finished
//                        return true;
//                    }

                    String myAge = et.getText().toString();
                    mkToast(myAge);
                    return true;
                }
                //i have not finished
                //still listening
                return false;
            }
        });

    }



    /**
     * This will show a
     * flashing alert
     * @param text
     */
    public void mkToast(String text)
    {
        Context con = getApplicationContext();
        Toast t = Toast.makeText(con,text,Toast.LENGTH_SHORT);
        t.show();
    }

    public void doit11(View v)
    {
        mkToast("doit1");
    }
    public void doit222(View v)
    {
        mkToast("doit2");
    }

    //causes error
    public void onRadioButtonClicked(View v)
    {
        RadioButton button = (RadioButton)v;
        boolean checked = button.isChecked();
        switch(v.getId())
        {
            case R.id.cel1:
                if(checked)
                {
                    doit11(v);
                }
            break;
            case R.id.far1:
                if(checked)
                {
                    doit222(v);
                }
                break;
        }
    }


}
