package com.cs211d.week5application;

import android.app.Activity;
import android.os.Bundle;

public class Week5Application extends Activity
{
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
    }
}
