package cs211d.com.intent;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Window;


public class Intent2 extends Activity {


//        provide fully qualified name of application
    Intent i = new Intent(getApplicationContext(),HelpActivity.class);

    //we did not place context herr
    Intent i2 = new Intent("com.cs211d.intent.action.HelpActivity");



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);

        setContentView(R.layout.activity_intent);

        // startActivity(i2);
        startActivity(i2);
    }


    /**
     * we overright the methdd
     * two codes will comeback to you
     * request code is the code you used
     * @param requestCode
     * @param resultCode
     */
    private void onActivityResult(int requestCode, int resultCode) {
        //fist line in callback is super
        super.onActivityResult(requestCode,resultCode,i);

        //check if request code is the same as yours
        if(requestCode == 12 && resultCode== Activity.RESULT_OK)
        {

        }

        // if request cod is the same the result code
        // will be from the class you requested from

        //the resultcode will tell if it has found your
        //resource and inform us why it couldnt do it

        //result codes can be the following:
        /*
            Activity.RESULT_OK
            Activity.RESULT_CANCELD // user stoped

            if android does not know acitvity info
            it will not let you call them, so you
            provide information in android and you
            place them into androidmanifets.xml
        */

    }


    public void inPhoneOrEmulator()
    {
        String android_id = Settings.Secure.getString(
                getContentResolver(), Settings.Secure.ANDROID_ID);
        //System.Enviroment.DeviceType != DeviceType.Emulator

//        getApplicationContext().
        if(false)
        {
            Log.e("Tag", "Emmulator is not running");
        }

        if("google_sdk".equals(Build.PRODUCT))
        {
            //you are in cellphones
            Log.e("Tag", "Emmulator is not running");
        }

        if(android_id == null)
        {
            Log.e("Tag", "you are inside emulator");
        }else{
            Log.e("Tag", "you are inside cell");
        }
    }

    public boolean isEmulator()
    {
        return Build.MANUFACTURER.equals("unknown");
    }
//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.menu_intent, menu);
//        return true;
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        // Handle action bar item clicks here. The action bar will
//        // automatically handle clicks on the Home/Up button, so long
//        // as you specify a parent activity in AndroidManifest.xml.
//        int id = item.getItemId();
//
//        //noinspection SimplifiableIfStatement
//        if (id == R.id.action_settings) {
//            return true;
//        }
//
//        return super.onOptionsItemSelected(item);
//    }
}
