package cs211d.com.intent;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Window;

/**
 * Created by edgar on 2/26/15.
 */
public class HelpActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);

        setContentView(R.layout.acitivity_intentnew);

//        create object intent
//        Intent i = new Intent(this,HelpActivity.class);

        //to know if another activity hass call this
        //activity use this

        if(this.getIntent() == null)
        {
            Log.d("test", "no activity has called me");
        }else{
            Log.d("test", "some activity has called me");
        }

    }
    /**
     * when you send to a provider and you have
     * a code, they return back information based
     * on the code, requestcod is the id
     * based on id you will send back  what i am requesting
     * @param i the intent
     * @param requestCode the number you want
     */
    public void startAcitivyForResult(Intent i, int requestCode)
    {
        // by using intent or bundle you can send information
        // when job is completed it should tell you when it is
        // done imageprovider will call that method
        // activty will be notified by a call back

        // callback lets activity know it is done

    }
}
