package cs211d.com.understandintents;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;


public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_main);

        TextView tv = new TextView(this);
        tv.setText("hello android. say hello");
        setContentView(tv);



        Intent intent;
        intent = this.getIntent();

        if(intent == null)
        {
            Log.d("test tag", "This activity is invoked without an intent");
        }else{
            Log.d("test tag", "This activity is invoked with an intent: ");
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.menu_main, menu);
//        return true;
        super.onCreateOptionsMenu(menu);
        int base = Menu.FIRST;
        MenuItem item1 = menu.add(base,base,base,"TEST");
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == 1)
        {
//            IntentUtils.tryOneOfThese(this);

//            this works -- generic
//            String actionName = "android.intent.action.BASICVIEWACTIVITY";
//            Intent intent = new Intent(actionName);

//*************** USING BUNDLES
//            // place bundle in intent
//            Bundle anotherBundle = new Bundle();
//            anotherBundle.putString("keyvalue","mybundlestring");
//            anotherBundle.putInt("thenumber!",332324234);
//            intent.putExtras(anotherBundle);
//            startActivity(intent);
//*************** USING BUNDLES

//            this works -- explicity
//            Intent currentIntent = new Intent(getApplicationContext(), BasicViewActivity.class);
//            startActivity(currentIntent);

////            using components (explictly)

//            does not work
//            Intent intent = new Intent();
//            intent.setComponent(new ComponentName(
//                    "com.android.contacts",
//                    "com.android.contacts.DialContactsEntryActivity"
//            ));
//            startActivity(intent);

// using categories
                Intent intent = new Intent(getApplicationContext(),BasicViewActivity.class);
                startActivity(intent);

        }
        else{
            return super.onOptionsItemSelected(item);
        }
        return true;
    }
}
