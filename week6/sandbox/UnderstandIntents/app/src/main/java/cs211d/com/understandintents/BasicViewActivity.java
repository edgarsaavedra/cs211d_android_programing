package cs211d.com.understandintents;

import android.app.Activity;
import android.content.res.Resources;
import android.os.Bundle;
import android.content.Intent;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by edgar on 3/1/15.
 */
public class BasicViewActivity extends Activity {
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.some_view);

        Intent intent;
        intent = this.getIntent();

//*************** USING BUNDLES
//        get the bundle from an intent
//        Bundle extraBundle = (Bundle)intent.getExtras();
//
//
//        TextView tv = (TextView)findViewById(R.id.textviewnew);
//        tv.setText(extraBundle.getString("keyvalue")+" "+extraBundle.getInt("thenumber!"));
//*************** USING BUNDLES


//
//        if(intent == null)
//        {
//            Log.d("test tag", "This activity is invoked without an intent");
//        }else{
//            Log.d("test tag", "This activity is invoked with an intent: ");
//        }
    }
//    public static void invokeMyApplication(Activity parentActivity)
//    {
//        String actionName = "cs211d.com.understandingintents.intent.action.ShowBasicView";
//        Intent intent = new Intent(actionName);
//        parentActivity.startActivity(intent);
//    }


}
