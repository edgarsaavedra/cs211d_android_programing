package com.cs211d.myfirstapp;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.ContentResolver;
import android.content.res.AssetManager;
import android.content.res.Resources;
import android.content.res.XmlResourceParser;
import android.database.Cursor;
import android.graphics.drawable.GradientDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.provider.ContactsContract.Contacts;
import android.util.Log;
import android.widget.TextView;
import android.provider.ContactsContract;

public class MyAndroidAppActivity extends Activity
{
    /** Called when the activity is first created. */
    @SuppressLint("NewApi")
	@Override
    public void onCreate(Bundle savedInstanceState)
    {
    	
    	
    	Uri peopleBaseUri = ContactsContract.Contacts.CONTENT_URI;
    	Uri myPersonUri = Uri.withAppendedPath(peopleBaseUri, "23");
    	try {
        	Cursor cur = managedQuery(myPersonUri, null, null, null, null);
		} catch (Exception e) {
			// TODO: handle exception
			Log.d("error: ",e+"");
		}
    	
    	Log.d("PeopleBaseUri ", ContactsContract.Contacts.CONTENT_URI+"");
    	Log.d("Photo contact uri", Contacts.PHOTO_THUMBNAIL_URI);
    	Log.d("CONTENT_URI contact uri", Contacts.CONTENT_URI+"");
    	
    	String[] projection = new String[] {
    			Contacts._ID,
    			Contacts.DISPLAY_NAME_PRIMARY
    	};
    	
    	Uri mContactsUri = ContactsContract.Contacts.CONTENT_URI;
    	
    	// best way to retrieve a query; returns a manged query.
    	Cursor managedCurso = managedQuery(mContactsUri, projection, null, null, Contacts.DISPLAY_NAME_PRIMARY+" ASC");
    	
    	//walking through a cursor
    	if(managedCurso.moveToFirst() == false)
    	{
    		//no rows empty cursor
    	}else
    	{
    		int nameColumnIndex = managedCurso.getColumnIndex(Contacts.DISPLAY_NAME_PRIMARY);
//    		int numColumnIndex = managedCurso.getColumnIndex(Contacts.Data.DATA1);
    		String name = managedCurso.getString(nameColumnIndex);
//    		String number = managedCurso.getString(numColumnIndex);
    		Log.d("Cursor name: ", name); 
//    	    Log.d("Cursor Data1: ", number);
    	}
    	
//    	/**
//    	 * The contact content provider
//    	 */
//    		// contact data
//    		String columnsToExtract[] = new String[] {
//    				Contacts._ID, Contacts.DISPLAY_NAME, Contacts.PHOTO_THUMBNAIL_URI
//    		};
//    		// get the content resolver, which handles all of the queries
//    		ContentResolver contentResolver = getContentResolver();
//    		
//           // filter the contacts with empty names
//    		String whereClause = "(("+Contacts.DISPLAY_NAME + "NOTNULL) AND ("+
//    				Contacts.DISPLAY_NAME + " != '' ) AND (" + Contacts.STARRED +
//    				"== 1))";
//    		//sort by increasing ID
//    		String sortOrder = Contacts._ID + " ASC";
//    		
//    		//query constacts ContentProvider
//    		Cursor cursor = contentResolver.query(Contacts.CONTENT_URI, columnsToExtract, whereClause, null, sortOrder);
//    		
//    		Log.d("the cursor: ",cursor+"");
//    		
//    		//pass cursor to custom list adapter
//            //setListAdapter(new ContactInfoListAdapter(this, R.layout.list_item, cursor,0));
//    		
//    	/**
//    	 * END: The contact content provider
//    	 */
    	
    	
        TextView tv = (TextView)this.findViewById(R.id.edgars_text);
//        tv.setText("sdfsdf");
     	
    	Resources res = this.getResources();
    	XmlResourceParser xpp = res.getXml(R.xml.test);
    	String strings[] = res.getStringArray(R.array.test_array);
    	
    	for(String s : strings)
    	{
    		Log.d("examp: ",s);
    	}
    	
    	
    	int mainBgColor = this.getResources().getColor(R.color.main_bg_color);
    	Log.d("the bg color: ", mainBgColor+"");
    	
    	try {
			Log.d("xml parser", getEventsFromAnXMLFile(this));
		} catch (XmlPullParserException e) {
			// TODO Auto-generated catch bluock
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	
    	try {
			Log.d("read raw",getStringFromRawFile(this));
		} catch (Exception e) {
			// TODO: handle exception
		}
    	
    	GradientDrawable roundedRectangle = (GradientDrawable)this.getResources().getDrawable(R.drawable.my_rounded_rectangle);
//    	tv.setBackgroundDrawable(roundedRectangle);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main2);
    }
    
    private String getEventsFromAnXMLFile(Activity activity)
    throws XmlPullParserException, IOException
    {
    	StringBuffer sb = new StringBuffer();
    	Resources res = activity.getResources();
    	XmlResourceParser xpp = res.getXml(R.xml.test);
    	
    	xpp.next();
    	int eventType = xpp.getEventType();
    	while (eventType != XmlPullParser.END_DOCUMENT)
    	{
    		if(eventType == XmlPullParser.START_DOCUMENT)
    		{
    			sb.append("*******Start Documnet");
    		}else if (eventType == XmlPullParser.START_TAG) {
				sb.append("\nStart tag "+xpp.getName());
			}else if (eventType == XmlPullParser.END_TAG) {
				sb.append("\nEnd tag"+xpp.getName());
			}else if (eventType == XmlPullParser.TEXT) {
				sb.append("\nText "+xpp.getText());
			}
    		eventType = xpp.next();
    	}
    	sb.append("\n******END DOCUMENT");
    	return sb.toString();    			
    }
    
    String getStringFromRawFile(Activity activity)
    throws IOException
    {
    	Resources r = activity.getResources();
    	InputStream is = r.openRawResource(R.raw.textfile);
    	String myText = converStreamToString(is);
    	is.close();
    	return myText;
    }
    
    String converStreamToString(InputStream is)
    throws IOException
    {
    	ByteArrayOutputStream baos = new ByteArrayOutputStream();
    	int i = is.read();
    	while(i != -1)
    	{
    		baos.write(i);
    		i = is.read();
    	}
    	return baos.toString();
    }
   
//    read from asset file, assets do not have ids and need full path
    String getStringFromAssetFile(Activity activity) throws IOException
    {
    	AssetManager am = activity.getAssets();
    	InputStream is = am.open("asset.txt");
    	String s = converStreamToString(is);
    	is.close();
    	return s;
    }
}
